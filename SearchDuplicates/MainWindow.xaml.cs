﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.Threading;
using System.Windows.Forms;
using System.ComponentModel;

namespace SearchDuplicates
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string directoryPath;
        public List<string> filesPaths;
        SearchDuplicatesFiles search = new SearchDuplicatesFiles();

        public MainWindow()
        {
            InitializeComponent();
            filesPaths = new List<string>();
            directoryPath = PathBox.Text;
        }

        private void BrowseButton_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog { SelectedPath = @"D:\" };
            dialog.ShowDialog();

            PathBox.Text = dialog.SelectedPath;
            directoryPath = dialog.SelectedPath;
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            Update(directoryPath);

            if (ResultList.Items.Count > 0)
                DeleteButton.IsEnabled = true;
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (ResultList.SelectedItem != null)
            {
                List<string> filesToDelete = new List<string>();
                foreach (var x in ResultList.SelectedItems)
                    filesToDelete.Add(filesPaths.First(f => f.Contains(x.ToString())));

                search.DeleteFiles(filesToDelete);

                Update(directoryPath);
            }
            else
            {
                search.DeleteFiles(filesPaths);

                Update(directoryPath);
            }

            if (ResultList.Items.Count == 0) DeleteButton.IsEnabled = false;
        }

        private void GenerateButton_Click(object sender, RoutedEventArgs e)
        {
            FileGenerator generator = new FileGenerator();
            generator.Generation(directoryPath);
            Update(directoryPath);
        }

        [System.ComponentModel.Browsable(false)]
        //public bool InvokeRequired { get; }
        //public object Invoke(Delegate method)

        public void Update(string directoryPath)
        {
            filesPaths.Clear();

            StatusBar.Content = "Wait...";

            SearchButton.IsEnabled = false;
            DeleteButton.IsEnabled = false;
            GenerateButton.IsEnabled = false;
            BrowseButton.IsEnabled = false;

            new Thread(() =>
            {
                filesPaths = search.SearchFiles(directoryPath).ToList();

                Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Background,
                   new Action(() =>
                   {
                       ResultList.Items.Clear();
                       filesPaths.ForEach(x => ResultList.Items.Add(System.IO.Path.GetFileName(x)));

                       StatusBar.Content = "Done!";

                       SearchButton.IsEnabled = true;
                       DeleteButton.IsEnabled = true;
                       GenerateButton.IsEnabled = true;
                       BrowseButton.IsEnabled = true;
               }));
            }).Start();

           
            /*
            new Thread(() =>
           {
               Action action = () =>
               {
                   SearchButton.IsEnabled = false;
                   DeleteButton.IsEnabled = false;
                   GenerateButton.IsEnabled = false;
                   BrowseButton.IsEnabled = false;

                   filesPaths = search.SearchFiles(directoryPath).ToList();
                   ResultList.Items.Clear();
                   filesPaths.ForEach(x => ResultList.Items.Add(System.IO.Path.GetFileName(x)));

                   StatusBar.Content = "Done!";

                   SearchButton.IsEnabled = true;
                   DeleteButton.IsEnabled = true;
                   GenerateButton.IsEnabled = true;
                   BrowseButton.IsEnabled = true;
               };

               if (InvokeRequired)
                   Invoke(action);
               else
                   action();
               /*
               Invoke((MethodInvoker)(() =>
               {
                   StatusBar.Content = "Wait...";

                   SearchButton.IsEnabled = false;
                   DeleteButton.IsEnabled = false;
                   GenerateButton.IsEnabled = false;
                   BrowseButton.IsEnabled = false;

                   filesPaths = search.SearchFiles(directoryPath).ToList();
                   ResultList.Items.Clear();
                   filesPaths.ForEach(x => ResultList.Items.Add(System.IO.Path.GetFileName(x)));

                   StatusBar.Content = "Done!";

                   SearchButton.IsEnabled = true;
                   DeleteButton.IsEnabled = true;
                   GenerateButton.IsEnabled = true;
                   BrowseButton.IsEnabled = true;
               }));*/
           //}).Start();

        }
        /*
        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            TestProgressBar.Value = e.ProgressPercentage;
            ProgressTextBlock.Text = (string)e.UserState;
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            var worker = sender as BackgroundWorker;
            worker.ReportProgress(0, String.Format("Processing Iteration 1."));
            for (int i = 0; i < 10; i++)
            {

                filesPaths.Clear();
                filesPaths = search.SearchFiles(directoryPath).ToList();
                ResultList.Items.Clear();
                filesPaths.ForEach(x => ResultList.Items.Add(System.IO.Path.GetFileName(x)));
                worker.ReportProgress((i + 1) * 10, String.Format("Processing Iteration {0}.", i + 2));
            }

            worker.ReportProgress(100, "Done Processing.");
        }

        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            System.Windows.MessageBox.Show("All Done!");
            TestProgressBar.Value = 0;
            ProgressTextBlock.Text = "";
        }
        */

        private void ResultList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var count = (e.Source as System.Windows.Controls.ListView).SelectedItems.Count;

            if (count > 0)
            {
                if (ResultList.Items.Count > 0) DeleteButton.IsEnabled = true;
                DeleteButton.Content = "Delete(" + count.ToString() + ") Files";
            }
            else
                DeleteButton.Content = "Delete ALL Files";
        }
    }
}
