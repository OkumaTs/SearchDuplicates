﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchDuplicates
{
    class FileGenerator
    {
        private const Int32 FileCountPerDir = 50;
        private const Int32 DirLevel = 0;
        private const Int32 DirCountPerDir = 0;
        private const Int32 FileMaxSizeBytes = 4_194_304;


        public void Generation(string directoryPath)
        {
            CreateDirIfNeeded(directoryPath);
            GenerateTestSet(directoryPath);
        }

        private static void GenerateTestSet(string directoryPath)
        {
            GenerateDirTree(directoryPath, 0);
        }

        private static void GenerateDirTree(string dir, int level)
        {
            if (level > DirLevel)
            {
                return;
            }

            CreateFiles(dir);

            level++;

            for (int i = 0; i < DirCountPerDir; i++)
            {
                var current = Path.Combine(dir, $"dir{i}");
                Directory.CreateDirectory(current);
                GenerateDirTree(current, level);
            }
        }

        private static void CreateFiles(string dir)
        {
            for (int i = 0; i < FileCountPerDir; i++)
            {
                var name = Path.Combine(dir, $"file{i}.txt");
                Random random = new Random();

                var content = new Byte[random.Next(FileMaxSizeBytes)];
                random.NextBytes(content);
                File.WriteAllBytes(name, content);
                Console.WriteLine(name);
            }
        }

        private static void CreateDirIfNeeded(string dir)
        {
            if (Directory.Exists(dir))
                return;

            Directory.CreateDirectory(dir);
        }
    }
}
