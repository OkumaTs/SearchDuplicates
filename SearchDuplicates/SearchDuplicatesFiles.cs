﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security.Cryptography;
using System.Windows;

namespace SearchDuplicates
{
    class SearchDuplicatesFiles
    {
        public IEnumerable<string> SearchFiles(string directoryPath)
        {
            var allFiles = Directory.GetFiles(directoryPath, "*.*", SearchOption.AllDirectories);

            var dupFiles = allFiles.Select(f =>
            {
                using (var fs = new FileStream(f, FileMode.Open, FileAccess.Read))
                {
                    return new
                    {
                        FileName = f,
                        FileHash = BitConverter.ToString(SHA1.Create().ComputeHash(fs)),
                        DateModified = File.GetLastWriteTime(f)
                    };
                }
            })
            .GroupBy(f => f.FileHash)
            .Select(g => new { FileHash = g.Key, Files = g.OrderBy(j => j.DateModified).Select(z => z.FileName).ToList() })
            .SelectMany(f => f.Files.Skip(1)).ToList();

            return dupFiles;
        }


        public void DeleteFiles(List<string> filesPaths)
        {
            try
            {
                filesPaths.ForEach(x => { File.Delete(x); });
            }
            catch (UnauthorizedAccessException e) { MessageBox.Show(e.Message); }
        }
    }
}
